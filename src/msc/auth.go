package msc

import (
	"errors"
	"net"

	"golang.org/x/crypto/ssh"
	"set"
	"sshd"
)

var ErrNotWhitelisted = errors.New("not whitelisted")

func newAuthKey(key ssh.PublicKey) string {
	if key == nil {
		return ""
	}
	return sshd.Fingerprint(key)
}

func newAuthItem(key ssh.PublicKey, comment string) set.Item {
	return set.Itemize(newAuthKey(key), comment)
}

func newAuthAddr(addr net.Addr) string {
	if addr == nil {
		return ""
	}
	host, _, _ := net.SplitHostPort(addr.String())
	return host
}

type Auth struct {
	whitelist *set.Set
}

func NewAuth() *Auth {
	return &Auth{
		whitelist: set.New(),
	}
}

func (a *Auth) Check(addr net.Addr, key ssh.PublicKey, clientVersion string) error {
	authkey := newAuthKey(key)

	if a.whitelist.Len() != 0 {
		whitelisted := a.whitelist.In(authkey)
		if !whitelisted {
			return ErrNotWhitelisted
		}
		return nil
	}

	return ErrNotWhitelisted
}

func (a *Auth) Whitelist(key ssh.PublicKey, comment string) {
	if key == nil {
		return
	}
	authItem := newAuthItem(key, comment)
	a.whitelist.Set(authItem)
	logger.Debugf("Added to whitelist: %q", authItem.Key())
}

func (a *Auth) ClearWhitelist() {
	a.whitelist.Clear()
}

func (a *Auth) GetName(key ssh.PublicKey) string {
	item, _ := a.whitelist.Get(newAuthKey(key))
	return item.Value().(string)
}
