package set

import (
	"errors"
	"strings"
	"sync"
)

var ErrCollision = errors.New("key already exists")
var ErrMissing = errors.New("item does not exist")
var ErrNil = errors.New("item value must not be nil")

type IterFunc func(key string, item Item) error

type Set struct {
	sync.RWMutex
	lookup    map[string]Item
	normalize func(string) string
}

func New() *Set {
	return &Set{
		lookup:    map[string]Item{},
		normalize: normalize,
	}
}

func (s *Set) Clear() int {
	s.Lock()
	n := len(s.lookup)
	s.lookup = map[string]Item{}
	s.Unlock()
	return n
}

func (s *Set) Len() int {
	s.RLock()
	defer s.RUnlock()
	return len(s.lookup)
}

func (s *Set) In(key string) bool {
	key = s.normalize(key)
	s.RLock()
	item, ok := s.lookup[key]
	s.RUnlock()
	if ok && item.Value() == nil {
		s.cleanup(key)
		ok = false
	}
	return ok
}

func (s *Set) Get(key string) (Item, error) {
	key = s.normalize(key)
	s.RLock()
	item, ok := s.lookup[key]
	s.RUnlock()

	if !ok {
		return nil, ErrMissing
	}
	if item.Value() == nil {
		s.cleanup(key)
	}

	return item, nil
}

func (s *Set) cleanup(key string) {
	s.Lock()
	item, ok := s.lookup[key]
	if ok && item == nil {
		delete(s.lookup, key)
	}
	s.Unlock()
}

func (s *Set) Add(item Item) error {
	if item.Value() == nil {
		return ErrNil
	}
	key := s.normalize(item.Key())

	s.Lock()
	defer s.Unlock()

	oldItem, found := s.lookup[key]
	if found && oldItem.Value() != nil {
		return ErrCollision
	}

	s.lookup[key] = item
	return nil
}

func (s *Set) Set(item Item) error {
	if item.Value() == nil {
		return ErrNil
	}
	key := s.normalize(item.Key())

	s.Lock()
	defer s.Unlock()
	s.lookup[key] = item
	return nil
}

func (s *Set) Remove(key string) error {
	key = s.normalize(key)

	s.Lock()
	defer s.Unlock()

	_, found := s.lookup[key]
	if !found {
		return ErrMissing
	}
	delete(s.lookup, key)
	return nil
}

func (s *Set) Each(fn IterFunc) error {
	var err error
	s.RLock()
	for key, item := range s.lookup {
		if item.Value() == nil {
			defer s.cleanup(key)
			continue
		}
		if err = fn(key, item); err != nil {
			break
		}
	}
	s.RUnlock()
	return err
}

func (s *Set) ListPrefix(prefix string) []Item {
	r := []Item{}
	prefix = s.normalize(prefix)

	s.Each(func(key string, item Item) error {
		if strings.HasPrefix(key, prefix) {
			r = append(r, item)
		}
		return nil
	})

	return r
}

func normalize(key string) string {
	return strings.ToLower(key)
}
