package message

import (
	"fmt"
	"strings"
	"time"
)

type Message interface {
	Render(*Theme) string
	String() string
	Command() string
	Timestamp() time.Time
}

func ParseInput(body string, from *User) Message {
	m := NewPublicMsg(body, from)
	cmd, isCmd := m.ParseCommand()
	if isCmd {
		return cmd
	}
	return m
}

type Msg struct {
	body      string
	timestamp time.Time
}

func NewMsg(body string) *Msg {
	return &Msg{
		body:      body,
		timestamp: time.Now(),
	}
}

func (m Msg) Render(t *Theme) string {
	return m.String()
}

func (m Msg) String() string {
	return m.body
}

func (m Msg) Command() string {
	return ""
}

func (m Msg) Timestamp() time.Time {
	return m.timestamp
}

type PublicMsg struct {
	Msg
	from *User
}

func NewPublicMsg(body string, from *User) PublicMsg {
	return PublicMsg{
		Msg: Msg{
			body:      body,
			timestamp: time.Now(),
		},
		from: from,
	}
}

func (m PublicMsg) From() *User {
	return m.from
}

func (m PublicMsg) ParseCommand() (*CommandMsg, bool) {
	if !strings.HasPrefix(m.body, "/") {
		return nil, false
	}

	fields := strings.Fields(m.body)
	command, args := fields[0], fields[1:]
	msg := CommandMsg{
		PublicMsg: m,
		command:   command,
		args:      args,
	}
	return &msg, true
}

func (m PublicMsg) Render(t *Theme) string {
	if t == nil {
		return m.String()
	}

	return fmt.Sprintf("%s: %s", t.ColorName(m.from), m.body)
}

func (m PublicMsg) RenderFor(cfg UserConfig) string {
	if cfg.Highlight == nil || cfg.Theme == nil {
		return m.Render(cfg.Theme)
	}

	if !cfg.Highlight.MatchString(m.body) {
		return m.Render(cfg.Theme)
	}

	body := cfg.Highlight.ReplaceAllString(m.body, cfg.Theme.Highlight("${1}"))
	if cfg.Bell {
		body += Bel
	}
	return fmt.Sprintf("%s: %s", cfg.Theme.ColorName(m.from), body)
}

func (m PublicMsg) RenderSelf(cfg UserConfig) string {
	return fmt.Sprintf("[%s] %s", cfg.Theme.ColorName(m.from), m.body)
}

func (m PublicMsg) String() string {
	return fmt.Sprintf("%s: %s", m.from.Name(), m.body)
}

type SystemMsg struct {
	Msg
	to *User
}

func NewSystemMsg(body string, to *User) *SystemMsg {
	return &SystemMsg{
		Msg: Msg{
			body:      body,
			timestamp: time.Now(),
		},
		to: to,
	}
}

func (m *SystemMsg) Render(t *Theme) string {
	if t == nil {
		return m.String()
	}
	return t.ColorSys(m.String())
}

func (m *SystemMsg) String() string {
	return fmt.Sprintf("-> %s", m.body)
}

func (m *SystemMsg) To() *User {
	return m.to
}

type AnnounceMsg struct {
	Msg
}

func NewAnnounceMsg(body string) *AnnounceMsg {
	return &AnnounceMsg{
		Msg: Msg{
			body:      body,
			timestamp: time.Now(),
		},
	}
}

func (m AnnounceMsg) Render(t *Theme) string {
	if t == nil {
		return m.String()
	}
	return t.ColorSys(m.String())
}

func (m AnnounceMsg) String() string {
	return fmt.Sprintf(" * %s", m.body)
}

type CommandMsg struct {
	PublicMsg
	command string
	args    []string
}

func (m CommandMsg) Command() string {
	return m.command
}

func (m CommandMsg) Args() []string {
	return m.args
}

func (m CommandMsg) Body() string {
	return m.body
}
