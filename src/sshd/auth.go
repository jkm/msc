package sshd

import (
	"crypto/sha256"
	"fmt"
	"net"

	"golang.org/x/crypto/ssh"
	"internal/sanitize"
)

type Auth interface {
	Check(net.Addr, ssh.PublicKey, string) error
}

func MakeAuth(auth Auth) *ssh.ServerConfig {
	return &ssh.ServerConfig{
		NoClientAuth: false,
		PublicKeyCallback: func(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
			err := auth.Check(conn.RemoteAddr(), key, sanitize.Data(string(conn.ClientVersion()), 64))
			if err != nil {
				return nil, err
			}
			perm := &ssh.Permissions{Extensions: map[string]string{
				"pubkey": string(key.Marshal()),
			}}
			return perm, nil
		},
	}
}

func Fingerprint(k ssh.PublicKey) string {
	return fmt.Sprintf("%x", sha256.Sum256(k.Marshal()))
}
