CHROOT_DIR := tmp

DEBIAN_ARCH := arm64v8
DEBIAN_VERSION := bullseye
DEBIAN_URL := https://raw.githubusercontent.com/debuerreotype/docker-debian-artifacts/dist-$(DEBIAN_ARCH)/$(DEBIAN_VERSION)/rootfs.tar.xz

GOLANG_ARCH := arm64
GOLANG_VERSION := 1.15.15
GOLANG_URL := https://dl.google.com/go/go$(GOLANG_VERSION).linux-$(GOLANG_ARCH).tar.gz

LDFLAGS := -extldflags -static


msc: chroot
	chroot $(CHROOT_DIR) apt-get update
	chroot $(CHROOT_DIR) apt-get install curl git patch -y
	curl $(GOLANG_URL) -o $(CHROOT_DIR)/go.tar.gz
	tar xf $(CHROOT_DIR)/go.tar.gz -C $(CHROOT_DIR)/usr/local
	mkdir $(CHROOT_DIR)/go
	cp -r src $(CHROOT_DIR)/go
	chroot $(CHROOT_DIR) env GOPATH=/go /usr/local/go/bin/go get -d main
	patch -d $(CHROOT_DIR)/go -p1 < patch/terminal.go.patch
	chroot $(CHROOT_DIR) env GOPATH=/go CGO_ENABLED=0 /usr/local/go/bin/go build -o /usr/local/bin/msc -ldflags "$(LDFLAGS)" main

chroot: clean
	mkdir $(CHROOT_DIR)
	curl $(DEBIAN_URL) -o debian.tar.xz
	tar xf debian.tar.xz -C $(CHROOT_DIR)

install:
	cp $(CHROOT_DIR)/usr/local/bin/msc /usr/local/bin

build:
	GOPATH=$(shell pwd) go build -o bin/msc -ldflags "$(LDFLAGS)" main

patch: get
	patch -s src/golang.org/x/crypto/ssh/terminal/terminal.go patch/terminal.go.patch

get:
	GOPATH=$(shell pwd) go get -d main

image: clean
	docker build -t msc:latest .

lint:
	gofmt -s -w src

clean:
	rm -rf $(CHROOT_DIR)
	rm -f debian.tar.xz
	rm -f bin/msc && rm -rf src/{github.com,golang.org}
