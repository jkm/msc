package chat

import (
	"errors"
	"fmt"
	"strings"

	"chat/message"
)

var ErrInvalidCommand = errors.New("invalid command")
var ErrNoOwner = errors.New("command without owner")
var ErrMissingArg = errors.New("missing argument")
var ErrMissingPrefix = errors.New("command missing prefix")

type Command struct {
	Prefix     string
	PrefixHelp string
	Help       string

	Handler func(*Room, message.CommandMsg) error
}

type Commands map[string]*Command

func (c Commands) Add(cmd Command) error {
	if cmd.Prefix == "" {
		return ErrMissingPrefix
	}

	c[cmd.Prefix] = &cmd
	return nil
}

func (c Commands) Run(room *Room, msg message.CommandMsg) error {
	if msg.From() == nil {
		return ErrNoOwner
	}

	cmd, ok := c[msg.Command()]
	if !ok {
		return ErrInvalidCommand
	}

	return cmd.Handler(room, msg)
}

func (c Commands) Help() string {
	commands := []*Command{}
	for _, cmd := range c {
		commands = append(commands, cmd)
	}

	help := "Available commands:" + message.Newline + NewCommandsHelp(commands).String()
	return help
}

var timeformatDatetime = "2006-01-02 15:04:05"
var timeformatTime = "15:04"

var defaultCommands *Commands

func init() {
	defaultCommands = &Commands{}
	InitCommands(defaultCommands)
}

func InitCommands(c *Commands) {
	c.Add(Command{
		Prefix: "/help",
		Handler: func(room *Room, msg message.CommandMsg) error {
			room.Send(message.NewSystemMsg(room.commands.Help(), msg.From()))
			return nil
		},
	})

	c.Add(Command{
		Prefix: "/exit",
		Help:   "Exit the chat.",
		Handler: func(room *Room, msg message.CommandMsg) error {
			msg.From().Close()
			return nil
		},
	})

	c.Add(Command{
		Prefix: "/list",
		Help:   "List users who are connected.",
		Handler: func(room *Room, msg message.CommandMsg) error {
			theme := msg.From().Config().Theme

			colorize := func(u *message.User) string {
				return theme.ColorName(u)
			}

			if theme == nil {
				colorize = func(u *message.User) string {
					return u.Name()
				}
			}

			names := room.Members.ListPrefix("")
			colNames := make([]string, len(names))
			for i, uname := range names {
				colNames[i] = colorize(uname.Value().(*Member).User)
			}

			body := fmt.Sprintf("%d connected: %s", len(colNames), strings.Join(colNames, ", "))
			room.Send(message.NewSystemMsg(body, msg.From()))
			return nil
		},
	})

	c.Add(Command{
		Prefix:     "/theme",
		PrefixHelp: "[colors|...]",
		Help:       "Set your color theme.",
		Handler: func(room *Room, msg message.CommandMsg) error {
			user := msg.From()
			args := msg.Args()
			cfg := user.Config()
			if len(args) == 0 {
				theme := "plain"
				if cfg.Theme != nil {
					theme = cfg.Theme.ID()
				}
				var output strings.Builder
				fmt.Fprintf(&output, "Current theme: %s%s", theme, message.Newline)
				fmt.Fprintf(&output, "   Themes available: ")

				for i, t := range message.Themes {
					output.WriteString(t.ID())
					if i < len(message.Themes)-1 {
						output.WriteString(", ")
					}
				}
				room.Send(message.NewSystemMsg(output.String(), user))
				return nil
			}

			id := args[0]
			for _, t := range message.Themes {
				if t.ID() == id {
					cfg.Theme = &t
					user.SetConfig(cfg)
					body := fmt.Sprintf("Set theme: %s", id)
					room.Send(message.NewSystemMsg(body, user))
					return nil
				}
			}
			return errors.New("theme not found")
		},
	})

	c.Add(Command{
		Prefix: "/shrug",
		Handler: func(room *Room, msg message.CommandMsg) error {
			room.Send(message.NewPublicMsg(`¯\_(ツ)_/¯`, msg.From()))
			return nil
		},
	})

	c.Add(Command{
		Prefix:     "/whois",
		PrefixHelp: "user",
		Help:       "Information about user.",
		Handler: func(room *Room, msg message.CommandMsg) error {
			args := msg.Args()
			if len(args) == 0 {
				return errors.New("must specify user")
			}

			member, ok := room.GetMember(args[0])
			if !ok {
				return errors.New("user not found")
			}

			room.Send(message.NewSystemMsg(member.User.Identity.Whois(), msg.From()))

			return nil
		},
	})
}
