package msc

import (
	"fmt"
	"io"
	"sync"

	"chat"
	"chat/message"
	"sshd"
)

const maxInputLength int = 1024

func GetPrompt(user *message.User) string {
	name := user.Name()
	cfg := user.Config()
	if cfg.Theme != nil {
		name = cfg.Theme.ColorName(user)
	}
	return fmt.Sprintf("[%s] ", name)
}

type Host struct {
	*chat.Room
	listener *sshd.SSHListener
	commands chat.Commands
	auth     *Auth

	Version string

	theme message.Theme
	mu    sync.Mutex
}

func NewHost(listener *sshd.SSHListener, auth *Auth) *Host {
	room := chat.NewRoom()
	h := Host{
		Room:     room,
		listener: listener,
		commands: chat.Commands{},
		auth:     auth,
	}

	chat.InitCommands(&h.commands)
	room.SetCommands(h.commands)

	go room.Serve()
	return &h
}

func (h *Host) SetTheme(theme message.Theme) {
	h.mu.Lock()
	h.theme = theme
	h.mu.Unlock()
}

func (h *Host) Connect(term *sshd.Terminal) {
	id := message.NewIdentity(term.Conn)
	id.SetName(h.auth.GetName(id.PublicKey()))

	user := message.NewUserScreen(*id, term)

	cfg := user.Config()
	cfg.Theme = &h.theme
	user.SetConfig(cfg)

	go user.Consume()

	defer user.Close()
	defer term.Close()

	_, err := h.Join(user)
	if err != nil {
		logger.Errorf("[%s] Failed to join: %s", term.Conn.RemoteAddr(), err)
		return
	}

	term.SetPrompt(GetPrompt(user))
	user.SetHighlight(user.Name())

	logger.Debugf("[%s] Joined: %s", term.Conn.RemoteAddr(), user.Name())

	for {
		line, err := term.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			logger.Errorf("[%s] Terminal reading error: %s", term.Conn.RemoteAddr(), err)
			break
		}

		if len(line) > maxInputLength {
			user.Send(message.NewSystemMsg("Message rejected: Input too long.", user))
			continue
		}
		if line == "" {
			term.Write([]byte{})
			continue
		}

		m := message.ParseInput(line, user)
		if m, ok := m.(*message.CommandMsg); ok {
			user.HandleMsg(m)
		}

		h.HandleMsg(m)

		cmd := m.Command()
		if cmd == "/theme" {
			term.SetPrompt(GetPrompt(user))
			user.SetHighlight(user.Name())
		}
	}

	err = h.Leave(user)
	if err != nil {
		logger.Errorf("[%s] Failed to leave: %s", term.Conn.RemoteAddr(), err)
		return
	}
	logger.Debugf("[%s] Leaving: %s", term.Conn.RemoteAddr(), user.Name())
}

func (h *Host) Serve() {
	h.listener.HandlerFunc = h.Connect
	h.listener.Serve()
}
