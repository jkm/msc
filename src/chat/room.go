package chat

import (
	"errors"
	"fmt"
	"io"
	"sync"

	"chat/message"
	"internal/humantime"
	"set"
	"sshd"
)

const historyLen = 20
const roomBuffer = 10

var ErrAlreadyJoined = errors.New("already joined")
var ErrRoomClosed = errors.New("room closed")
var ErrInvalidUsername = errors.New("invalid username")

type Member struct {
	*message.User
}

type Room struct {
	history   *message.History
	broadcast chan message.Message
	commands  Commands
	closed    bool
	closeOnce sync.Once

	Members    *set.Set
	PublicKeys *set.Set
}

func NewRoom() *Room {
	broadcast := make(chan message.Message, roomBuffer)

	return &Room{
		broadcast: broadcast,
		history:   message.NewHistory(historyLen),
		commands:  *defaultCommands,

		Members:    set.New(),
		PublicKeys: set.New(),
	}
}

func (r *Room) SetCommands(commands Commands) {
	r.commands = commands
}

func (r *Room) Close() {
	r.closeOnce.Do(func() {
		r.closed = true
		r.Members.Each(func(_ string, item set.Item) error {
			item.Value().(*Member).Close()
			return nil
		})
		r.Members.Clear()
		close(r.broadcast)
	})
}

func (r *Room) SetLogging(out io.Writer) {
	r.history.SetOutput(out)
}

func (r *Room) HandleMsg(m message.Message) {
	switch m := m.(type) {
	case *message.CommandMsg:
		cmd := *m
		err := r.commands.Run(r, cmd)
		if err != nil {
			m := message.NewSystemMsg(fmt.Sprintf("Err: %s", err), cmd.From())
			go r.HandleMsg(m)
		}
	case *message.SystemMsg:
		m.To().Send(m)
	default:
		r.history.Add(m)
		r.Members.Each(func(_ string, item set.Item) (err error) {
			user := item.Value().(*Member).User
			user.Send(m)
			return
		})
	}
}

func (r *Room) Serve() {
	for m := range r.broadcast {
		go r.HandleMsg(m)
	}
}

func (r *Room) Send(m message.Message) {
	r.broadcast <- m
}

func (r *Room) History(u *message.User) {
	for _, m := range r.history.Get(historyLen) {
		u.Send(m)
	}
}

func (r *Room) Join(u *message.User) (*Member, error) {
	if u.ID() == "" {
		u.Write([]byte("[ERROR] Invalid username\n"))
		return nil, ErrInvalidUsername
	}
	member := &Member{User: u}

	publicKey := u.Identity.PublicKey()
	fingerprint := sshd.Fingerprint(publicKey)

	if r.PublicKeys.In(fingerprint) {
		u.Write([]byte("[ERROR] Already joined\n"))
		return nil, ErrAlreadyJoined
	}

	err := r.Members.Add(set.Itemize(u.ID(), member))
	if err != nil {
		return nil, err
	}

	err = r.PublicKeys.Add(set.Itemize(fingerprint, publicKey))
	if err != nil {
		return nil, err
	}

	r.History(u)
	s := fmt.Sprintf("%s joined. (Connected: %d)", u.Name(), r.Members.Len())
	r.Send(message.NewAnnounceMsg(s))
	return member, nil
}

func (r *Room) Leave(u *message.User) error {
	publicKey := u.Identity.PublicKey()
	fingerprint := sshd.Fingerprint(publicKey)

	err := r.Members.Remove(u.ID())
	if err != nil {
		return err
	}

	err = r.PublicKeys.Remove(fingerprint)
	if err != nil {
		return err
	}

	s := fmt.Sprintf("%s left. (After %s)", u.Name(), humantime.Since(u.Joined()))
	r.Send(message.NewAnnounceMsg(s))
	return nil
}

func (r *Room) GetMember(id string) (*Member, bool) {
	m, err := r.Members.Get(id)
	if err != nil {
		return nil, false
	}

	return m.Value().(*Member), true
}
