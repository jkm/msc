package set

type Item interface {
	Key() string
	Value() interface{}
}

type item struct {
	key   string
	value interface{}
}

func (item *item) Key() string {
	return item.key
}

func (item *item) Value() interface{} {
	return item.value
}

func Itemize(key string, value interface{}) Item {
	return &item{key, value}
}

type StringItem string

func (item StringItem) Key() string {
	return string(item)
}

func (item StringItem) Value() interface{} {
	return true
}
