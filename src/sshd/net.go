package sshd

import (
	"net"
	"time"

	"golang.org/x/crypto/ssh"
)

type SSHListener struct {
	net.Listener
	config *ssh.ServerConfig

	HandlerFunc func(term *Terminal)
}

func ListenSSH(laddr string, config *ssh.ServerConfig) (*SSHListener, error) {
	socket, err := net.Listen("tcp4", laddr)
	if err != nil {
		return nil, err
	}
	l := SSHListener{Listener: socket, config: config}
	return &l, nil
}

func (l *SSHListener) handleConn(conn net.Conn) (*Terminal, error) {
	var handleTimeout = 20 * time.Second
	conn.SetReadDeadline(time.Now().Add(handleTimeout))
	defer conn.SetReadDeadline(time.Time{})

	sshConn, channels, requests, err := ssh.NewServerConn(conn, l.config)
	if err != nil {
		return nil, err
	}

	go ssh.DiscardRequests(requests)
	return NewSession(sshConn, channels)
}

func (l *SSHListener) Serve() {
	defer l.Close()
	for {
		conn, err := l.Accept()

		if err != nil {
			logger.Printf("Failed to accept connection: %s", err)
			break
		}

		go func() {
			term, err := l.handleConn(conn)
			if err != nil {
				logger.Printf("[%s] Failed to handshake: %s", conn.RemoteAddr(), err)
				conn.Close()
				return
			}
			l.HandlerFunc(term)
		}()
	}
}
