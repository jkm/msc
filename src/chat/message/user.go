package message

import (
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"sync"
	"time"

	"sshd"
)

const messageBuffer = 5
const messageTimeout = 5 * time.Second
const reHighlight = `\b(%s)\b`
const timestampTimeout = 30 * time.Minute

var ErrUserClosed = errors.New("user closed")

type User struct {
	Identity
	colorIdx int
	joined   time.Time
	msg      chan Message
	done     chan struct{}

	term      *sshd.Terminal
	closeOnce sync.Once

	mu      sync.Mutex
	config  UserConfig
	replyTo *User
	lastMsg time.Time
}

func NewUser(identity Identity) *User {
	u := User{
		Identity: identity,
		config:   DefaultUserConfig,
		joined:   time.Now(),
		msg:      make(chan Message, messageBuffer),
		done:     make(chan struct{}),
	}
	u.setColorIdx(rand.Int())

	return &u
}

func NewUserScreen(identity Identity, term *sshd.Terminal) *User {
	u := NewUser(identity)
	u.term = term
	return u
}

func (u *User) Joined() time.Time {
	return u.joined
}

func (u *User) Config() UserConfig {
	u.mu.Lock()
	defer u.mu.Unlock()
	return u.config
}

func (u *User) SetConfig(cfg UserConfig) {
	u.mu.Lock()
	u.config = cfg
	u.mu.Unlock()
}

func (u *User) SetID(id string) {
	u.Identity.SetID(id)
	u.setColorIdx(rand.Int())
}

func (u *User) ReplyTo() *User {
	u.mu.Lock()
	defer u.mu.Unlock()
	return u.replyTo
}

func (u *User) SetReplyTo(user *User) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.replyTo = user
}

func (u *User) setColorIdx(idx int) {
	u.colorIdx = idx
}

func (u *User) Close() {
	u.closeOnce.Do(func() {
		if u.term != nil {
			u.term.Close()
		}
		close(u.done)
	})
}

func (u *User) Consume() {
	for {
		select {
		case <-u.done:
			return
		case m, ok := <-u.msg:
			if !ok {
				return
			}
			u.HandleMsg(m)
		}
	}
}

func (u *User) ConsumeOne() Message {
	return <-u.msg
}

func (u *User) HasMessages() bool {
	select {
	case msg := <-u.msg:
		u.msg <- msg
		return true
	default:
		return false
	}
}

func (u *User) SetHighlight(s string) error {
	re, err := regexp.Compile(fmt.Sprintf(reHighlight, s))
	if err != nil {
		return err
	}
	u.mu.Lock()
	u.config.Highlight = re
	u.mu.Unlock()
	return nil
}

func (u *User) render(m Message) string {
	cfg := u.Config()
	var out string
	switch m := m.(type) {
	case PublicMsg:
		if u == m.From() {
			if !cfg.Echo {
				return ""
			}
			out += m.RenderSelf(cfg)
		} else {
			out += m.RenderFor(cfg)
		}
	case *CommandMsg:
		out += m.RenderSelf(cfg)
	default:
		out += m.Render(cfg.Theme)
	}
	if cfg.Timeformat != nil {
		ts := m.Timestamp()
		if cfg.Timezone != nil {
			ts = ts.In(cfg.Timezone)
		} else {
			ts = ts.UTC()
		}
		return cfg.Theme.Timestamp(ts.Format(*cfg.Timeformat)) + "  " + out + Newline
	}
	return out + Newline
}

func (u *User) Write(data []byte) error {
	_, err := u.term.Write(data)
	if err != nil {
		logger.Printf("Write failed to %s, closing: %s", u.Name(), err)
		u.Close()
	}
	return err
}

func (u *User) WriteMsg(m Message) error {
	r := u.render(m)
	_, err := u.term.Write([]byte(r))
	if err != nil {
		logger.Printf("Write failed to %s, closing: %s", u.Name(), err)
		u.Close()
	}
	return err
}

func (u *User) HandleMsg(m Message) error {
	u.mu.Lock()
	u.lastMsg = m.Timestamp()
	u.mu.Unlock()
	return u.WriteMsg(m)
}

func (u *User) Send(m Message) error {
	select {
	case <-u.done:
		return ErrUserClosed
	case u.msg <- m:
	case <-time.After(messageTimeout):
		logger.Printf("Message buffer full, closing: %s", u.Name())
		u.Close()
		return ErrUserClosed
	}
	return nil
}

type UserConfig struct {
	Highlight  *regexp.Regexp
	Bell       bool
	Echo       bool
	Timeformat *string
	Timezone   *time.Location
	Theme      *Theme
}

var DefaultUserConfig UserConfig

func init() {
	DefaultUserConfig = UserConfig{
		Bell: true,
		Echo: true,
	}
}
