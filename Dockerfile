FROM debian:buster-slim

ENV GOLANG_VERSION 1.14.8

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ca-certificates curl git patch && \
    rm -rf /var/lib/apt/lists/* && \
    curl --silent https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz -o go.tar.gz && \
    tar xzf go.tar.gz -C /usr/local && \
    rm go.tar.gz

ENV GOPATH /go
ENV GOBIN $GOPATH/bin
ENV PATH $GOBIN:/usr/local/go/bin:$PATH

WORKDIR $GOPATH

COPY src $GOPATH/src
COPY patch $GOPATH/patch

RUN mkdir $GOBIN && \
    chmod -R 777 $GOPATH && \
    go get -d main && \
    patch -s $GOPATH/src/golang.org/x/crypto/ssh/terminal/terminal.go $GOPATH/patch/terminal.go.patch && \
    go build -o $GOBIN/msc -ldflags "-extldflags -static" main && \
    rm -rf $GOPATH/src && \
    rm -rf $GOPATH/patch
