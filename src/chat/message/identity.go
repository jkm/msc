package message

import (
	"time"

	"internal/humantime"
	"internal/sanitize"
	"sshd"
)

type Identity struct {
	sshd.Connection
	id      string
	created time.Time
}

func NewIdentity(conn sshd.Connection) *Identity {
	return &Identity{
		Connection: conn,
		id:         sanitize.Name(conn.Name()),
		created:    time.Now(),
	}
}

func (i Identity) ID() string {
	return i.id
}

func (i *Identity) SetID(id string) {
	i.id = id
}

func (i *Identity) SetName(name string) {
	i.SetID(name)
}

func (i Identity) Name() string {
	return i.id
}

func (i Identity) Whois() string {
	fingerprint := sshd.Fingerprint(i.PublicKey())
	return "Username:           " + i.Name() + Newline +
		" > SHA256 fingerprint: " + fingerprint + Newline +
		" > SSH client:         " + sanitize.Data(string(i.ClientVersion()), 64) + Newline +
		" > Joined:             " + humantime.Since(i.created) + " ago"
}
