package main

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"os/user"
	"strings"
	"syscall"

	"github.com/alexcesaro/log"
	"github.com/alexcesaro/log/golog"
	"github.com/jessevdk/go-flags"
	"golang.org/x/crypto/ssh"

	"chat"
	"chat/message"
	"msc"
	"sshd"
)

var Version string = "1.0"

type Options struct {
	Bind      string `short:"b" long:"bind" description:"Host and port to listen on." default:"0.0.0.0:22"`
	Debug     bool   `short:"d" long:"debug" description:"Enable debug logging."`
	Identity  string `short:"i" long:"identity" description:"Private key to identify server with." default:"~/.msc/id_ed25519"`
	Log       string `short:"l" long:"log" description:"Write chat log to this file." default:"~/.msc/history"`
	Version   bool   `short:"v" long:"version" description:"Show version and exit."`
	Whitelist string `short:"w" long:"whitelist" description:"File of public keys who are allowed to connect." default:"~/.msc/whitelist"`
}

func fail(code int, format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(code)
}

func parseWhitelist(whitelistPath string, auth *msc.Auth) error {
	if strings.HasPrefix(whitelistPath, "~/") {
		user, err := user.Current()
		if err == nil {
			whitelistPath = strings.Replace(whitelistPath, "~", user.HomeDir, 1)
		}
	}

	auth.ClearWhitelist()

	err := fromFile(whitelistPath, func(line []byte) error {
		key, comment, _, _, err := ssh.ParseAuthorizedKey(line)
		if err != nil {
			return err
		}

		if comment == "" {
			fail(9, "Empty comment in %s\n", whitelistPath)
		}

		auth.Whitelist(key, comment)
		return nil
	})

	return err
}

func main() {
	options := Options{}
	parser := flags.NewParser(&options, flags.Default)
	p, err := parser.Parse()
	if err != nil {
		if p == nil {
			fmt.Print(err)
		}
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
		return
	}

	if options.Version {
		fmt.Println(Version)
		os.Exit(0)
	}

	if options.Debug {
		msc.SetLogger(golog.New(os.Stderr, log.Debug))
		chat.SetLogger(os.Stderr)
		sshd.SetLogger(os.Stderr)
		message.SetLogger(os.Stderr)
	} else {
		msc.SetLogger(golog.New(os.Stderr, log.Warning))
	}

	privateKeyPath := options.Identity
	if strings.HasPrefix(privateKeyPath, "~/") {
		user, err := user.Current()
		if err == nil {
			privateKeyPath = strings.Replace(privateKeyPath, "~", user.HomeDir, 1)
		}
	}

	signer, err := ReadPrivateKey(privateKeyPath)
	if err != nil {
		fail(3, "Failed to read identity private key: %v\n", err)
	}

	auth := msc.NewAuth()
	config := sshd.MakeAuth(auth)
	config.AddHostKey(signer)

	s, err := sshd.ListenSSH(options.Bind, config)
	if err != nil {
		fail(4, "Failed to listen on socket: %v\n", err)
	}
	defer s.Close()

	fmt.Printf("Listening for connections on %v\n", s.Addr().String())

	host := msc.NewHost(s, auth)
	host.SetTheme(message.Themes[0])
	host.Version = Version

	whitelistPath := options.Whitelist
	err = parseWhitelist(whitelistPath, auth)
	if err != nil {
		fail(6, "Failed to load whitelist: %v\n", err)
	}

	logPath := options.Log
	if strings.HasPrefix(logPath, "~/") {
		user, err := user.Current()
		if err == nil {
			logPath = strings.Replace(logPath, "~", user.HomeDir, 1)
		}
	}

	fp, err := os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fail(8, "Failed to open log file for writing: %v", err)
	}
	host.SetLogging(fp)

	go host.Serve()

	exit := make(chan int)
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGUSR1)

	go func() {
		for {
			switch <-sig {
			case syscall.SIGINT:
				fmt.Fprintln(os.Stderr, "Interrupt signal detected, shutting down.")
				exit <- 0
			case syscall.SIGUSR1:
				fmt.Fprintln(os.Stderr, "SIGUSR1 signal detected, rereading whitelist.")
				parseWhitelist(whitelistPath, auth)
			}
		}
	}()

	exitCode := <-exit
	os.Exit(exitCode)
}

func fromFile(path string, handler func(line []byte) error) error {
	if path == "" {
		return nil
	}

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		err := handler(scanner.Bytes())
		if err != nil {
			return err
		}
	}
	return nil
}
