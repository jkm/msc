package message

import (
	"fmt"
)

const (
	Reset = "\033[0m"

	Bold = "\033[1m"

	Dim = "\033[2m"

	Italic = "\033[3m"

	Underline = "\033[4m"

	Blink = "\033[5m"

	Invert = "\033[7m"

	Newline = "\r\n"

	Bel = "\007"
)

type Style interface {
	String() string
	Format(string) string
}

type style string

func (c style) String() string {
	return string(c)
}

func (c style) Format(s string) string {
	return c.String() + s + Reset
}

type Color256 uint8

func (c Color256) String() string {
	return fmt.Sprintf("38;05;%d", c)
}

func (c Color256) Format(s string) string {
	return "\033[" + c.String() + "m" + s + Reset
}

func Color256Palette(colors ...uint8) *Palette {
	size := len(colors)
	p := make([]Style, 0, size)
	for _, color := range colors {
		p = append(p, Color256(color))
	}
	return &Palette{
		colors: p,
		size:   size,
	}
}

type Color0 struct{}

func (c Color0) String() string {
	return ""
}

func (c Color0) Format(s string) string {
	return s
}

type Palette struct {
	colors []Style
	size   int
}

func (p Palette) Get(i int) Style {
	if p.size == 1 {
		return p.colors[0]
	}
	return p.colors[i%(p.size-1)]
}

func (p Palette) Len() int {
	return p.size
}

func (p Palette) String() string {
	r := ""
	for _, c := range p.colors {
		r += c.Format("X")
	}
	return r
}

type Theme struct {
	id        string
	sys       Style
	pm        Style
	highlight Style
	names     *Palette
}

func (theme Theme) ID() string {
	return theme.id
}

func (theme Theme) ColorName(u *User) string {
	if theme.names == nil {
		return u.Name()
	}

	return theme.names.Get(u.colorIdx).Format(u.Name())
}

func (theme Theme) ColorSys(s string) string {
	if theme.sys == nil {
		return s
	}

	return theme.sys.Format(s)
}

func (theme Theme) Highlight(s string) string {
	if theme.highlight == nil {
		return s
	}
	return theme.highlight.Format(s)
}

func (theme Theme) Timestamp(s string) string {
	if theme.sys == nil {
		return s
	}
	return theme.sys.Format(s)
}

var Themes []Theme

var DefaultTheme *Theme

var MonoTheme *Theme

func allColors256() *Palette {
	colors := []uint8{}
	var i uint8
	for i = 0; i < 255; i++ {
		colors = append(colors, i)
	}
	return Color256Palette(colors...)
}

func readableColors256() *Palette {
	colors := []uint8{}
	var i uint8
	for i = 0; i < 255; i++ {
		if i == 0 || i == 7 || i == 8 || i == 15 || i == 16 || i == 17 || i > 230 {
			continue
		}
		colors = append(colors, i)
	}
	return Color256Palette(colors...)
}

func init() {
	Themes = []Theme{
		{
			id:        "colors",
			names:     readableColors256(),
			sys:       Color256(245),
			pm:        Color256(7),
			highlight: style(Bold + "\033[48;5;11m\033[38;5;16m"),
		},
		{
			id:        "solarized",
			names:     Color256Palette(1, 2, 3, 4, 5, 6, 7, 9, 13),
			sys:       Color256(11),
			pm:        Color256(15),
			highlight: style(Bold + "\033[48;5;3m\033[38;5;94m"),
		},
		{
			id:        "hacker",
			names:     Color256Palette(82),
			sys:       Color256(22),
			pm:        Color256(28),
			highlight: style(Bold + "\033[48;5;22m\033[38;5;46m"),
		},
		{
			id: "mono",
		},
	}

	DefaultTheme = &Themes[0]
	MonoTheme = &Themes[3]
}

func printTheme(t Theme) {
	fmt.Println("Printing theme:", t.ID())
	if t.names != nil {
		for i, color := range t.names.colors {
			fmt.Printf("%s ", color.Format(fmt.Sprintf("name%d", i)))
		}
		fmt.Println("")
	}
	fmt.Println(t.ColorSys("SystemMsg"))
	fmt.Println(t.Highlight("Highlight"))
	fmt.Println("")
}

func printPalette(p *Palette) {
	for i, color := range p.colors {
		fmt.Printf("%d\t%s\n", i, color.Format(color.String()+" "))
	}
}
