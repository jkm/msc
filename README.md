# msc

msc is a minimal fork of [ssh-chat](https://github.com/shazow/ssh-chat)

### What distinguishes msc from the ssh-chat?

- key whitelist based authorization only (no anonymous login)
- no nick renaming
- no op/admin roles
- no banning functionality of any kind (IP, client etc.)
- significantly less chat commands
- less bloat
